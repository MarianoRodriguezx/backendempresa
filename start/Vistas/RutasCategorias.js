const Route = use('Route')

Route.group(() => { 

    Route.resource('categorias', 'CategoriaController').apiOnly()
    Route.get('allIndex', 'CategoriaController.allIndex')
    
 }).namespace('Vistas').prefix('api/v1').middleware(['auth'])