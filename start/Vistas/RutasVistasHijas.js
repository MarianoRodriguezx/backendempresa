const Route = use('Route')

Route.group(() => { 

    Route.resource('vistas', 'VistaHijaController').apiOnly()

    Route.get('prueba', 'VistaHijaController.prueba')
    Route.get('contar/:id', 'VistaHijaController.contar')

 }).prefix('api/v1').namespace('Vistas')