'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VistaHija extends Model {
    static get table(){
        return 'vista_hijas'
    }

    //primary y foreign

    prueba(){
        return this.hasMany('App/Models/CategoriaRol', 'categoria', 'categoria')
    }

    cats(){
        return this.belongsTo('App/Models/Categoria', 'categoria', 'id')
    }

    vistas(){
        return this.hasMany('App/Models/Categoria', 'categoria', 'id')
    }

    /* vistas(){
        return this.hasMany('App/Models/VistasHijasRole')
    } */

    //-----------------------------------------------------------------------------//

    vista()
    {
        return this.hasMany('App/Models/VistasHijasRole')
    }

   /*  vista2()
    {
        return this.hasMany('App/Models/VistasHijasRole')
    } */
}

module.exports = VistaHija
