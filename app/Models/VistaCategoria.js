'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VistaCategoria extends Model {
    static get table(){
        return 'vista_categorias'
    }

    static get store(){
        return [
            'vista',
            'rol'
        ]
    }
}

module.exports = VistaCategoria
