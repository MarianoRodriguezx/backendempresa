'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Categoria extends Model {

    static get table(){
        return 'categorias'
    }

    static get store(){
        return [
            'nombre',
            'icono'
        ]
    }

    cats(){
        return this.hasMany('App/Models/CategoriaRol', 'id', 'categoria')
    }

    cats(){
        return this.hasMany('App/Models/VistaHija', 'id', 'categoria')
    }

    prueba(){
        return this.belongsTo('App/Models/VistaHija', 'id', 'categoria')
    }

    vistas(){
        return this.hasMany('App/Models/VistaHija', 'id', 'categoria')
    }

    //___________________________________________________________________________//

    vista2()
    {
        return this.manyThrough('App/Models/VistaHija', 'vista')
    }

}

module.exports = Categoria
