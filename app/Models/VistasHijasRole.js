'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class VistasHijasRole extends Model {
    static get table(){
        return 'vistas_hijas_roles'
    }

    /* prueba()
    {
        return this.hasMany('App/Models/')
    } */

    vista()
    {
        return this.hasMany('App/Models/VistaHija', 'vista_id', 'id')
    }
}

module.exports = VistasHijasRole
