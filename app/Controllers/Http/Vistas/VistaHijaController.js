'use strict'

const Database = require("@adonisjs/lucid/src/Database")
const VistaHija = use("App/Models/VistaHija")
const Categoria = use("App/Models/Categoria")
const Drive = use('Drive')


/**
 * Resourceful controller for interacting with vistahijas
 */
class VistaHijaController {
  
  async index ({ request, response }) {

    const vh = await VistaHija.query().with('cats').groupBy('nombre').fetch()

    //await vh.load('cats')

    response.ok({message: "consulta correcta", data: vh})
    //console.log(lista)
    //return "x"

  }

  async store ({ request, response }) {
    try
    {
      await VistaHija.create(request.all())

      response.ok()
    }
    catch(error)
    {
      response.badRequest()
    }
  }

  async prueba({ response }){ 
    const p = await VistaHija.all()
    Drive.put('hello.docx', Buffer.from(JSON.stringify(p)))
  }

  async show ({ params, request, response, view }) {
    try
    {
      const vistas = await VistaHija.query().where('rol', params.id).fetch()

      response.ok({message: "Consulta Correcta", data:  vistas})
    }
    catch(error)
    {
      response.badRequest()
    }
  }

  async contar ({ params, response }){
    /* try
    { */
      const vistas = await VistaHija.query().count('id').where('rol', params.id)/*  Database().select('id')  *//* .raw('SELECT COUNT(vista_hijas.id) FROM `vista_hijas` WHERE vista_hijas.rol=?'[params.id]) */

      response.ok({message: "Consulta Correcta", data: vistas})
    /* }
    catch(error)
    {
      response.badRequest()
    } */
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
    try
    {
      const vista = await VistaHija.findOrFail(params.id)

      vista.status=!vista.status

      vista.save()

      response.ok({message: "se desactivo correctamente"})
    }
    catch(error)
    {
      response.badRequest({message: "No existe la vista"})
    }
  }
}

module.exports = VistaHijaController
