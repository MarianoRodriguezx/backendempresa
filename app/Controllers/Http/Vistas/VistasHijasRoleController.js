'use strict'

const VistasHijasRole = use("App/Models/VistasHijasRole")


/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with vistashijasroles
 */
class VistasHijasRoleController {
  /**
   * Show a list of all vistashijasroles.
   * GET vistashijasroles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
    //try{

      const data = await VistasHijasRole.query().with('vista').with('vista2').fetch()

      response.ok({message: "Consulta Correcta", data:data})
    //}
    //catch(error){
    //  response.internalServerError({message: "Ocurrio un Error"})
    //}
  }

  /**
   * Render a form to be used for creating a new vistashijasrole.
   * GET vistashijasroles/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new vistashijasrole.
   * POST vistashijasroles
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single vistashijasrole.
   * GET vistashijasroles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing vistashijasrole.
   * GET vistashijasroles/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update vistashijasrole details.
   * PUT or PATCH vistashijasroles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a vistashijasrole with id.
   * DELETE vistashijasroles/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = VistasHijasRoleController
