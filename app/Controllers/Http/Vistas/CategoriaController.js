'use strict'

const Categoria = use("App/Models/Categoria")
const CategoriaRol = use('App/Models/CategoriaRol')


class CategoriaController {
  
  async index ({ response, auth }) {
    try{
      const categoria = await Categoria.query().with('vistas', (builder) => { builder.where('rol', auth.user.rol), builder.where('status', true)}).where('status', true).fetch()
      response.ok({message: "consulta correcta", data: categoria})
    }
    catch(error){
      response.badRequest({message: "ocurrio un error"})
    }
  }

  async allIndex({ response }){
    try
    {
      const categorias = await Categoria.all()
      response.ok({message: "correcto", data: categorias})
    }
    catch(error){
      response.badRequest({message: "ocurrio un error"})
    }
  }

  async store ({ request, response }) {
    //try{
      console.log(request.all())
      const cat = await Categoria.create(request.only(Categoria.store))
      response.ok({message: "Insertado correctamente", data: cat.id})
    //}
    //catch(error){
      
    //  response.badRequest({message: "ocurrio un error"})
    
  //}
  }

  async show ({ params, request, response }) {
  }

 
  async update ({ params, request, response }) {
  }

  async destroy ({ params, request, response }) {
    try{

      const categoria = await Categoria.findOrFail(params.id)
      categoria.status=!categoria.status
      categoria.save()
      response.ok({message: "Correcto", aux: categoria.status})
    }
    catch(error){
      response.badRequest({message: "No existe ninguna categoria con este ID"})
    }
  }
}

module.exports = CategoriaController
